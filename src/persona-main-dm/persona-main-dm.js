import {LitElement, html} from 'lit-element';
import '../persona-main/persona-main.js';
import '../persona-ficha-listado/persona-ficha-listado';
import '../persona-form/persona-form.js';

class PersonaMainDM extends LitElement{

   
    static get properties(){
        return{
            people: {type: Array}
        };
    }

    constructor(){

        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile:"Lorem ipsum dolor sit amet.",
                photo: {
                    src:"./img/descarga.jpg",
                    alt: "Ellen Ripley"
                }
            },
            {
                name: "Bruce Ripley",
                yearsInCompany: 5,
                profile:"Sed pharetra eros eu arcu volutpat venenatis.",
                photo: {
                    src:"./img/descarga2.jpg",
                    alt: "Bruce Ripley"
                }
            },
            {
                name: "Owen Rooce",
                yearsInCompany: 12,
                profile:"Vivamus in diam pellentesque, tempor dolor nec, posuere urna.",
                photo: {
                    src:"./img/descarga3.jpg",
                    alt: "Owen Rooce"
                }
            }
            ,
            {
                name: "Carlos Martinez",
                yearsInCompany: 7,
                profile:"Duis posuere erat eu tortor convallis porta.",
                photo: {
                    src:"./img/descarga4.jpg",
                    alt: "Owen Rooce"
                }
            }
            ,
            {
                name: "Steve Mckey",
                yearsInCompany: 2,
                profile:"Vivamus dignissim malesuada ex.", 
                photo: {
                    src:"./img/descarga5.jpg",
                    alt: "Owen Rooce"
                }
            }
        ];
       }

    updated(changedProperties){
        console.log("updated DM");

      if(changedProperties.has("people")){
           console.log("Ha cambiado people en persona-main dm");
        

           this.dispatchEvent(new CustomEvent("persona-main-dm",{
                detail:{
                    people: this.people
               }
            }))
       
        }

    }

 

}

customElements.define('persona-main-dm', PersonaMainDM)