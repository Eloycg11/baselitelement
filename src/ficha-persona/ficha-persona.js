import {LitElement, html} from 'lit-element';

class FichaPersona extends LitElement{

    static get properties(){
        return{
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo : {type: String}
        };
    }

    constructor(){

        super();

        this.name = "Prueba nombre";
        this.yearsInCompany = 12;

    }

    updated(changedProperties) {

        console.log("updated");

        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " +propName + " cambia valor, anterior era " +oldValue);
        });


        if (changedProperties.has("name")){
            console.log("name ha cambiado de valor, anterior era: "+changedProperties.get("name")
            +" nuevo es " +this.name);
        }
        if (changedProperties.has("yearsInCompany")){
            console.log("yearsInCompany ha cambiado de valor, anterior era: "+changedProperties.get("yearsInCompany")
            +" nuevo es " +this.yearsInCompany);
            this.updatePersonInfo();
        }

    }

    render(){
        return html`
            <div>
                <label>Nombre completo</label>
                <input type="text" id="fname" name="fname" value= "${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa</label>
                <input type="text"  name="yearsInCompany" value= "${this.yearsInCompany}" @input="${this.updateYearsInCompany}"
                ></input>
                <br />
                <input type="text"  value= "${this.personInfo}"  disabled></input>
                <br />
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("evento: " +e.target.value);
        this.name = e.target.value;
    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("evento: " +e.target.value);
        this.yearsInCompany = e.target.value;
    }
    updatePersonInfo(){
        console.log("updatePersonInfo");
  
        if (this.yearsInCompany>= 7){
            this.personInfo = "lead"
        }else if (this.yearsInCompany >= 5){
            this.personInfo = "senior"
        }else if (this.yearsInCompany >= 3){
            this.personInfo = "team"
        }else{
            this.personInfo =  "junior"
        }
        console.log("resultado personInfo: " +this.personInfo);
    }

}

customElements.define('ficha-persona', FichaPersona)